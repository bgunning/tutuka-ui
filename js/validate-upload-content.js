/**
 * Validate files select for upload
 * @param fileSelect - The File Select
 */
function validateFileTypes(fileSelect) {

    // var fileSelect = document.getElementById('file-select');
    var files = fileSelect.files;

    if (files.length != 2) {

        swal({
            title: 'File Count Error',
            text: "Please select two files for comparison",
            type: 'error',
            confirmButtonText: 'OK, let\'s try again!'
        }).then(function () {
            console.log("cleanup");
            uploadButton.innerHTML = 'Upload';
            fileSelect.value = '';
            // document.getElementById("file-select").value = "";
        })

        return;
    }


    var regex = new RegExp("(.*?)\.(csv)$");
    console.log("validating file types....."+fileSelect.value);

    for (var i = 0; i < files.length; i++) {

        var name= files.item(i).name;
        if (!(regex.test(name.toLowerCase()))) {
            swal({
                title: 'File Type Error',
                text: "Please select CSV files only",
                type: 'error',
                confirmButtonText: 'OK, let\'s try again!'
            }).then(function () {
                console.log("cleanup");
                uploadButton.innerHTML = 'Upload';
                fileSelect.value = '';
                // document.getElementById("file-select").value = "";

            });

            return;
        }

    }

}

