/**
 * Load summary result from fil upload
 * @param json_response - the server reply
 */
function loadSummaryTable(json_response){


    var mytable =$('#summaryTable').DataTable();
    var dataarray= new Array();
    var data1 = JSON.parse(json_response);
    dataarray.push(data1);
    mytable.clear();
    mytable.rows.add(dataarray);
    mytable.draw();

    var theId=data1['id'];
    var x = document.createElement("recordId");
    document.getElementsByName("recordId").value=theId;
    x=theId;

}


/**
 * Load Detailed comparison tables
 * @param json_response
 */
function loadTables(json_response) {


    console.trace("Load comparison tables....");
    var leftTable = $('#leftTableKeys').DataTable();
    ;
    var data1 = JSON.parse(json_response);


    var array1 = JSON.valueOf("potentialMatchesKeys");
    leftTable.clear();
    leftTable.rows.add(data1['potentialMatchesKeys']);
    leftTable.draw();

    //

    var rightTable = $('#rightTableValues').DataTable();
//            var data2 = JSON.parse(data13);
    rightTable.clear();
    rightTable.rows.add(data1['potentialMatchesValues']);
    rightTable.draw();


    var fileOneUnmatched = $('#fileOneUnmatched').DataTable();
//            var data2 = JSON.parse(data13);
    fileOneUnmatched.clear();
    fileOneUnmatched.rows.add(data1['fileOneUnmatched']);
    fileOneUnmatched.draw();

    var fileTwoUnmatched = $('#fileTwoUnmatched').DataTable();
//            var data2 = JSON.parse(data13);
    fileTwoUnmatched.clear();
    fileTwoUnmatched.rows.add(data1['fileTwoUnmatched']);
    fileTwoUnmatched.draw();


    var leftTableDuplicates = $('#fileOneDuplicates').DataTable();
//            var data2 = JSON.parse(data13);
    leftTableDuplicates.clear();
    leftTableDuplicates.rows.add(data1['fileOneDuplicates']);
    leftTableDuplicates.draw();

    var rightTableDuplicates = $('#fileTwoDuplicates').DataTable();
//            var data2 = JSON.parse(data13);
    rightTableDuplicates.clear();
    rightTableDuplicates.rows.add(data1['fileTwoDuplicates']);
    rightTableDuplicates.draw();


}
