/**
 * Send the request to the url
 * @param url
 */
function sendRequest(url) {

    // Set up the request.
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);


    // Set up a handler for when the request finishes.
    xhr.onreadystatechange = function () {
        if (xhr.status == 200 && xhr.readyState == 4) {
            loadTables(xhr.responseText);

        } else {
            console.log("status: " + xhr.status);
            console.log("readystate: " + xhr.readyState);
        }
    };

    // Send the Data.
    xhr.send();


}



/**
 * Post files to server for comparison
 * @param formData
 */
function postFiles(formData) {
    // Set up the request.
    var xhr = new XMLHttpRequest();
    // Open the connection.
    xhr.open('POST', '/tutuka/api/v1/upload/files', true);

    // xhr response handler
    xhr.onreadystatechange = function () {
        if (xhr.status == 200 && xhr.readyState == 4) {
            // File(s) uploaded.
            uploadButton.innerHTML = 'Upload';
            document.getElementById("file-select").value = "";
            swal(
                'Great job, your files are now stored',
                'Now select an algorithm for deeper analysis',
                'success'
            )
            loadSummaryTable(xhr.responseText);

        } else if (xhr.status == 400) {
            sessionStorage.setItem("errorMessage",xhr.responseText);
            window.location.replace("400.html")

        }
        else {
            console.trace("status: " + xhr.status);
            console.trace("readystate: " + xhr.readyState);
        }
    };

    // Send the Data.
    xhr.send(formData);


}
