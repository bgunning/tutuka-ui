# README #

This is the web front end for the [File Reconciliation Service](https://bitbucket.org/bgunning/tutuka-server/), a java client is also available [here](https://bitbucket.org/bgunning/tutuka-java-client/)

### Requirements ###

* Web server
* Reverse proxy configuration 
* Tutuka Server deployed on backend application server [details here](https://bitbucket.org/bgunning/tutuka-server/)

### How do I get set up? ###

* Clone the repo 
* Deploy on web server of your choice
* Configure reverse proxy rules

### Sample reverse proxy configuration required ###

This is a working reverse proxy config used with ```nginx``` web server

```
       location /tutuka/ {
                    proxy_buffering off;
                    proxy_connect_timeout 1800;
                    proxy_send_timeout 1800;
                    proxy_read_timeout 1800;
                    proxy_buffers 8 32k;
        
                    proxy_set_header Host $host;
                    proxy_set_header X-Real-IP $remote_addr;
                    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                    proxy_pass http://localhost:8080/tutuka/;
        
       }

```

### Running example ###
A running cloud image is available on request